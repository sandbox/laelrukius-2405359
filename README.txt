========================
drupal_naver_syndication
========================

This is Drupal 7 module that syndicate content to NAVER syndication service.


========================
Installation
========================

Install naver_syndication Module.
Log in at http://webmastertool.naver.com/
Add site & Get site bearer token key.
Go to drupal /admin/config/services/naver_syndication page.
Enter token key.
Run cron.
Syndication will occur in each cron job.
